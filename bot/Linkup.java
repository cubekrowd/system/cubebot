package discordscience.discordbot.plugin.cubekrowd;

import java.awt.Color;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import discordscience.discordbot.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.hooks.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class Linkup extends Plugin {

    public Linkup(Bot bot) {
        super(bot);
    }

    @Override
    public void onReady(ReadyEvent event) {
        getBot().getScheduler().scheduleWithFixedDelay(() -> {try {
            getBot().getJda().getGuilds().get(0).getMembers().forEach(member -> {
                updateUser(member);
            });
        } catch(Exception e) {
            log.error("Error while updating user!", e);
        }
                                                             }, 5, 5 * 60, TimeUnit.SECONDS);
        getBot().getScheduler().scheduleWithFixedDelay(() -> {try {
            long now = System.currentTimeMillis();
            for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", "unlink", now - (1000 * 10), now)) {
                String discordId = getDiscordId(ee.getData().get("u"), true);
                if(discordId == null) {
                    continue;
                }
                Member member = getBot().getJda().getGuilds().get(0).getMemberById(discordId);
                if(member != null) {
                    getBot().getJda().getGuilds().get(0).getController().removeRolesFromMember(member, member.getRoles()).queue();
                }
            }
        } catch(Exception e) {
            log.error("Error while chcking for unlink!", e);
        }
                                                             }, 5, 10, TimeUnit.SECONDS);


        getBot().getScheduler().schedule(() -> {
            EmbedBuilder eb = new EmbedBuilder().setTitle("\u26A0\uFE0F Broken Linkup");
            eb.setColor(Color.decode("#FFC107"));
            eb.setDescription("This is important as we currently have no way of automatically fixing your rank and nickname, it also allows some players to abuse and have multiple accounts.");
            eb.addField("Instructions:", "** **\n1. Do `!unlink` anywhere on the server.\n2. Do `/discord` from ingame and re-link.\n** **", false);
            eb.addField("Thank you.", "** **", true);
            eb.setFooter("V:1 T:" + System.currentTimeMillis(), null);
//            getBot().getJda().getUserById("92875770338213888").openPrivateChannel().complete().sendMessage(eb.build()).queue();
        }, 2, TimeUnit.SECONDS);
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        Message message = event.getMessage();
        if(message.getChannel().getId().equals(getBot().getConfig().getString("linkup-channel"))) {
            getBot().deleteMessage(message, 3, TimeUnit.MINUTES);
        }
    }

    @Override
    public boolean executeCommand(Message msg, String label, String[] args) {
        if(!label.equalsIgnoreCase("linkup")) {
            return false;
        }
        msg.delete().queue(); // delete their message

        // chck usage
        if(args.length == 0) {
            msg.getChannel().sendMessage(msg.getMember().getAsMention() + " \u2754 **Usage:** `!linkup <code>`").queue();
            return true;
        }

        String code = args[0].toLowerCase().replaceAll("[^a-z0-9]", "");
        code = code.substring(0, Math.min(code.length(), 4));
        long now = System.currentTimeMillis();
        if(EventStorageAPI.getStorage().getEntryStream("discordbot", "attempt", now - (1000 * 60 * 1), now).filter(ee -> ee.getData().get("d").equals(msg.getMember().getUser().getId())).count() > 3) {
            msg.getChannel().sendMessage(msg.getMember().getAsMention() + " \uD83D\uDED1 **Rate-limit!** You have tried too many times. Please wait a few minutes before trying again.").queue();
            return true;
        }
        EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "attempt", Map.of("c", code, "d", msg.getMember().getUser().getId())));

        // check if OK
        String uuid = null;

        long latest = -1;
        // only within last 2 minutes
        for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", "code", now - (1000 * 60 * 2), now)) {
            // skip if old
            if(ee.getTime() <= latest) {
                continue;
            }

            if(ee.getData().get("c").equals(code)) {
                // store newest time
                latest = ee.getTime();
                uuid = ee.getData().get("u");
            }
        }

        // check if matching code was foudn and if that user isn't already linked
        if(uuid == null) {
            msg.getChannel().sendMessage(msg.getMember().getAsMention() + " \uD83D\uDEAB **Invalid code!**").queue();
            return true;
        }
        if(getDiscordId(uuid) != null) {
            msg.getChannel().sendMessage(msg.getMember().getAsMention() + " \u274C **Already linked!**").queue();
            return true;
        }

        EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "linked", Map.of("u", uuid, "d", msg.getMember().getUser().getId())));
        updateUser(msg.getMember());
        msg.getChannel().sendMessage(msg.getMember().getAsMention() + " \u2b50 **Successfully linked!**").queue();
        return true;
    }

    public void updateUser(Member member) {
        // find Discord ID
        String uuid = getUniqueId(member);

        // skip processing those who are not linked
        if(uuid == null) {
            if(hasRole(member, getBot().getConfig().getString("linkup-role"))) {
                // TEMP
                if(!hasRole(member, getBot().getConfig().getString("434811770415349770"))) {
                    member.getGuild().getController().addRolesToMember(member, Arrays.asList(member.getGuild().getRoleById("434811770415349770"))).complete();
                    log.warn("Found user with Linked role but is not in database. User: {}", member.getUser().getId());
                }
            }
            return;
        }

        if(hasRole(member, getBot().getConfig().getString("434811770415349770"))) {
            member.getGuild().getController().removeRolesFromMember(member, Arrays.asList(member.getGuild().getRoleById("434811770415349770"))).complete();
            log.warn("Found user with Warning role but is linked. User: {}", member.getUser().getId());
        }

        List<String> roles = new ArrayList<>();
        String nick = null;

        long latest = -1;
        for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", Arrays.asList("code", "update"))) {
            // skip if old
            if(ee.getTime() <= latest) {
                continue;
            }

            if(ee.getData().get("u").equalsIgnoreCase(uuid)) {
                // store newest time
                latest = ee.getTime();

                roles.clear();
                roles.addAll(Arrays.asList(ee.getData().get("r").toLowerCase().split(",")));
                nick = ee.getData().get("n");
            }
        }

        List<String> ids = new ArrayList<>();
        List<String> full = new ArrayList<>();
        for(String part : getBot().getConfig().getString("discord-roles").split(";")) {
            String[] parts = part.split("=");
            full.addAll(Arrays.asList(parts[1].split(",")));
            if(roles.contains(parts[0].toLowerCase())) {
                ids.addAll(Arrays.asList(parts[1].split(",")));
            }
        }
        ids.add(getBot().getConfig().getString("linkup-role"));

        // Update roles
        boolean wrong = false;
        // filter so we only handle our own roles
        List<Role> currentRoles = member.getRoles().stream().filter(r -> full.contains(r.getId()) || r.getId().equals(getBot().getConfig().getString("linkup-role"))).collect(Collectors.toList());
        if(currentRoles.size() != ids.size()) {
            wrong = true;
        } else {
            for(int i = 0; !wrong && i != ids.size(); i++) {
                if(!ids.contains(currentRoles.get(i).getId())) {
                    wrong = true;
                }
            }
        }

        if(wrong) {
            member.getGuild().getController().removeRolesFromMember(member, currentRoles).complete();
            member.getGuild().getController().addRolesToMember(member, ids.stream().map(id -> member.getGuild().getRoleById(id)).collect(Collectors.toList())).complete();
        }

        // Update nick
        try {
            // Discord will reset nickname if it is the same as their real name
            if((member.getNickname() == null && !member.getUser().getName().equals(nick)) || (member.getNickname() != null && !nick.equals(member.getNickname()))) {
                log.debug("NICKSET " + nick + " " + member.getNickname() + " " + member.getUser().getName());
                member.getGuild().getController().setNickname(member, nick).complete();
	    }
        } catch(Exception e) {
            log.error("Failed to update nickname!", e);
        }
    }

    public boolean hasRole(Member member, String roleId) {
        for(Role r : member.getRoles()) {
            if(r.getId().equals(roleId)) {
                return true;
            }
        }
        return false;
    }

    public static String getUniqueId(Member member) {
        String uuid = null;
        long latest = -1;
        for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", Arrays.asList("linked", "unlink"))) {
            // skip if old
            if(ee.getTime() <= latest) {
                continue;
            }
            if(ee.getData().get("d").equalsIgnoreCase(member.getUser().getId())) {
                // store newest time
                latest = ee.getTime();
                if(ee.getType().equals("linked")) {
                    uuid = ee.getData().get("u");
                } else {
                    uuid = null; // if unlinked
                }
            }
        }
        return uuid;
    }

    public static String getDiscordId(String uuid) {
        return getDiscordId(uuid, false);
    }
    public static String getDiscordId(String uuid, boolean ignoreUnlink) {
        String id = null;
        long latest = -1;
        for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", Arrays.asList("linked", "unlink"))) {
            // skip if old
            if(ee.getTime() <= latest) {
                continue;
            }
            if(ee.getData().get("u").equalsIgnoreCase(uuid)) {
                // store newest time
                latest = ee.getTime();
                if(ee.getType().equals("linked")) {
                    id = ee.getData().get("d");
                } else {
                    if(!ignoreUnlink) {
                        id = null; // if unlinked
                    }
                }
            }
        }
        return id;
    }

}
