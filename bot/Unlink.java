package discordscience.discordbot.plugin.cubekrowd;

import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import discordscience.discordbot.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.hooks.*;
import net.dv8tion.jda.core.events.message.guild.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class Unlink extends Plugin {

    public Unlink(Bot bot) {
        super(bot);
    }

    @Override
    public boolean executeCommand(Message msg, String label, String[] args) {
        if(!label.equalsIgnoreCase("unlink")) {
            return false;
        }
        msg.delete().queue(); // delete their message

        // Always remove all roles anyways
        msg.getMember().getGuild().getController().removeRolesFromMember(msg.getMember(), msg.getMember().getRoles()).queue();

        // Get their MC UUID
        String uuid = Linkup.getUniqueId(msg.getMember());

        // check if matching code was found and if that user isn't already linked
        if(uuid == null) {
            msg.getChannel().sendMessage(msg.getMember().getAsMention() + " \uD83D\uDEAB **Not already linked!**").queue(m -> getBot().deleteMessage(m, 3, TimeUnit.SECONDS));
            return true;
        }

        EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "unlink", Map.of("u", uuid, "d", msg.getMember().getUser().getId())));
        msg.getChannel().sendMessage(msg.getMember().getAsMention() + " \uD83D\uDC4B **Unlinked!**").queue(m -> getBot().deleteMessage(m, 3, TimeUnit.SECONDS));
        return true;
    }

}
