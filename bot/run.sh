#!/bin/bash
cd discordbot

git reset --hard origin/master
git clean -fd

mkdir src/main/java/discordscience/discordbot/plugin/cubekrowd

cp ../Announce.java src/main/java/discordscience/discordbot/plugin/cubekrowd
cp ../Linkup.java src/main/java/discordscience/discordbot/plugin/cubekrowd
cp ../Unlink.java src/main/java/discordscience/discordbot/plugin/cubekrowd

if [ -f config.yml ]; then
  cp ../config.yml config.yml
else
  cp ../config-ck.yml config.yml
fi

mvn package && java -jar target/discordbot.jar
