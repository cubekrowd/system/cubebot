package discordscience.discordbot.plugin.cubekrowd;

import java.awt.Color;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import discordscience.discordbot.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.dv8tion.jda.core.*;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.events.*;
import net.dv8tion.jda.core.events.message.guild.*;
import net.dv8tion.jda.core.hooks.*;
import lombok.*;

@lombok.extern.log4j.Log4j2
public class Announce extends Plugin {

    public Announce(Bot bot) {
        super(bot);
    }

    @Override
    public void onReady(ReadyEvent event) {
        getBot().getScheduler().scheduleWithFixedDelay(() -> {
        File agd = new File("announce-going-down");
        if(agd.isFile()){
            agd.delete();
            getBot().getJda().getGuilds().get(0).getTextChannelById("195099343160606720").sendMessage("<:redlight:346200880707665921> CubeKrowd will be restarting for quick maintenance. Mainly back-end software updates. Should be back within 10-15 minutes!").complete();
        }
        File agu = new File("announce-going-up");
        if(agu.isFile()){
            agu.delete();
            getBot().getJda().getGuilds().get(0).getTextChannelById("195099343160606720").sendMessage("<:greenlight:346200862797856768> CubeKrowd is now back online!").complete();
        }
                                                             }, 2, 2, TimeUnit.SECONDS);
    }

}
