/*

    cubebot
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.cubebot;

import java.math.*;
import java.util.*;
import java.security.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.md_5.bungee.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.connection.*;
import net.md_5.bungee.api.config.*;
import net.md_5.bungee.api.plugin.*;
import lombok.*;

public class DiscordCommand extends Command {

    private final CubeBotPlugin plugin;
    private final String PREFIX = ChatColor.DARK_GRAY + "[" + ChatColor.DARK_AQUA + "C" + ChatColor.GOLD + "K" + ChatColor.BLUE + "Discord" + ChatColor.DARK_GRAY + "] ";

    public DiscordCommand(CubeBotPlugin plugin) {
        super("discord", "cubebot.linkup", new String[0]);
        this.plugin = plugin;
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (!(sender instanceof ProxiedPlayer)) {
            sender.sendMessage("This command can only be run by a player.");
            return;
        }
        ProxiedPlayer player = (ProxiedPlayer) sender;

        if(args.length == 0) {
            if(plugin.getDiscordId(player.getUniqueId().toString()) != null) {
                player.sendMessage(PREFIX + ChatColor.RED + "You have already linked your in-game account to Discord!");
                player.sendMessage(PREFIX + ChatColor.GRAY + "You can do " + ChatColor.AQUA + "/discord unlink" + ChatColor.GRAY + " to unlink your account.");
                return;
            }

            // check for rate-limit
            long now = System.currentTimeMillis();
            int count = 0;
            for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", "code", now - (1000 * 60 * 2), now)) {
                if(ee.getData().get("u").equalsIgnoreCase(player.getUniqueId().toString())) {
                    count++;
                }
            }

            if(count > 3) {
                player.sendMessage(PREFIX + ChatColor.RED + "You have generated too many codes recently. Please wait a bit before trying again.");
                return;
            }

            // generate and save code
            String roles = plugin.getRoles(player.getUniqueId());
            String code = generateCode();
            EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "code", Map.of("c", code, "r", roles, "u", player.getUniqueId().toString(), "n", player.getName())));

            player.sendMessage("");
            player.sendMessage(PREFIX + ChatColor.GREEN + "Your code is: " + ChatColor.GOLD + code);
            player.sendMessage(PREFIX + ChatColor.GREEN + "Now do " + ChatColor.AQUA + "!linkup " + code + ChatColor.GREEN + " in the " + ChatColor.BLUE + "#linkup" + ChatColor.GREEN + " chat!");
            player.sendMessage("");
            player.sendMessage(PREFIX + ChatColor.GREEN + "Link: " + ChatColor.GRAY + "https://discord.cubekrowd.net/");
            player.sendMessage("");
        } else if (args.length == 1 && args[0].equalsIgnoreCase("unlink")) {
            String discord = null;
            if((discord = plugin.getDiscordId(player.getUniqueId().toString())) == null) {
                player.sendMessage(PREFIX + ChatColor.RED + "You are not linked to any Discord account!");
                player.sendMessage(PREFIX + ChatColor.GRAY + "You can do " + ChatColor.AQUA + "/discord" + ChatColor.GRAY + " to link your account.");
                return;
            }
            EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "unlink", Map.of("u", player.getUniqueId().toString(), "d", discord)));
            player.sendMessage(PREFIX + ChatColor.GREEN + "Your account is no longer linked to Discord. Please do " + ChatColor.AQUA + "/discord" + ChatColor.GREEN + " to connect again.");
        } else {
            player.sendMessage(PREFIX + ChatColor.RED + "Unknown command: /discord " + String.join(" ", args));
        }
    }

    public String generateCode() {
        return new BigInteger(100, new SecureRandom()).toString(32).substring(0, 4);
    }

}
