/*

    cubebot
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.cubebot;

import java.util.*;
import java.util.concurrent.*;
import net.cubekrowd.eventstorageapi.api.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.connection.*;
import net.md_5.bungee.api.event.*;
import net.md_5.bungee.api.plugin.*;
import net.md_5.bungee.config.*;
import net.md_5.bungee.event.*;
import lombok.*;

@RequiredArgsConstructor
public class EventListener implements Listener {

    private final CubeBotPlugin plugin;

    @EventHandler
    public void onPostLogin(PostLoginEvent e) {
        CompletableFuture.runAsync(() -> {
            updatePlayer(e.getPlayer());
        });

    }

    private void updatePlayer(ProxiedPlayer player) {
        String uuid = player.getUniqueId().toString();
        // skip non-linked people
        if(plugin.getDiscordId(uuid) == null) {
            return;
        }

        String roles = null;
        String nick = null;

        long latest = -1;
        for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", Arrays.asList("code", "update"), 0, System.currentTimeMillis(), Map.of("u", uuid))) {
            // skip if old
            if(ee.getTime() <= latest) {
                continue;
            }

            // store newest time
            latest = ee.getTime();

            roles = ee.getData().get("r");
            nick = ee.getData().get("n");
        }

        // skip if data is the same
        String realRoles = plugin.getRoles(player.getUniqueId());
        if(!nick.equals(player.getName()) || !roles.equalsIgnoreCase(realRoles)) {
            EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "update", Map.of("r", realRoles, "u", uuid, "n", player.getName())));
        }

    }
}
