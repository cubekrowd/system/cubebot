/*

    cubebot
    Copyright (C) 2018  CubeKrowd Network

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.cubebot;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.stream.*;
import net.luckperms.api.*;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.node.types.InheritanceNode;
import net.cubekrowd.eventstorageapi.api.*;
import net.md_5.bungee.config.*;
import net.md_5.bungee.api.*;
import net.md_5.bungee.api.connection.*;
import net.md_5.bungee.api.plugin.*;
import net.md_5.bungee.config.*;
import org.json.simple.*;
import org.json.simple.parser.*;
import lombok.*;

public class CubeBotPlugin extends Plugin {

    @Override @SneakyThrows
    public void onEnable() {
        getProxy().getPluginManager().registerListener(this, new EventListener(this));
        getProxy().getPluginManager().registerCommand(this, new DiscordCommand(this));

        File file = new File(getDataFolder(), "config.yml");
        if(file.exists()) {
            Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            ((List<Map<String, Object>>) config.getList("data")).stream().forEach(m -> {
                try {
                    String uuid = (String) m.get("uuid");
                    String discord = (String) m.get("discordID");
                    String roles = String.join(",", ((List<String>) m.get("roles")));

                    HttpURLConnection connection = (HttpURLConnection) new URL("https://sessionserver.mojang.com/session/minecraft/profile/"+uuid.replace("-", "")).openConnection();
                    JSONObject response = (JSONObject) new JSONParser().parse(new InputStreamReader(connection.getInputStream()));
                    String name = (String) response.get("name");
                    if (name == null) {
                        getLogger().warning("name is null");
                        return;
                    }
                    getLogger().info(name);
                    EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "code", 1, Map.of("c", "0000", "r", roles, "u", uuid, "n", name)));
                    EventStorageAPI.getStorage().addEntry(new EventEntry("discordbot", "linked", 2, Map.of("u", uuid, "d", discord)));
                } catch (Exception ignored) {}
            });
        }
    }

    @Override
    public void onDisable() {
    }

    public String getDiscordId(String uuid) {
        String id = null;
        long latest = -1;
        for(EventEntry ee : EventStorageAPI.getStorage().getEntryList("discordbot", Arrays.asList("linked", "unlink"))) {
            // skip if old
            if(ee.getTime() <= latest) {
                continue;
            }
            if(ee.getData().get("u").equalsIgnoreCase(uuid)) {
                // store newest time
                latest = ee.getTime();
                if(ee.getType().equals("linked")) {
                    id = ee.getData().get("d");
                } else {
                    id = null; // if unlinked
                }
            }
        }
        return id;
    }

    @SneakyThrows
    public String getRoles(UUID uuid) {
        var api = LuckPermsProvider.get();
        return String.join(",", api.getUserManager()
                .loadUser(uuid)
                .get()
                .getNodes()
                .stream()
                .filter(n -> n.getType() == NodeType.INHERITANCE)
                .map(n -> ((InheritanceNode) n).getGroupName())
                .map(String::toLowerCase)
                .collect(Collectors.toList()));
    }

}
