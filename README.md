## Cubebot

**IMPORTANT! Both the discord bot and BungeeCord plugin in this
repository are now UNSUPPORTED. For up-to-date, working code, try
[CubeBot v2](https://gitlab.com/cubekrowd/system/cubebot2)
and the compatible BungeeCord plugin
[HeyCubeBot](https://gitlab.com/cubekrowd/plugins/heycubebot).**

--- 

Cubebot is a bot that connects the CubeKrowd discord with the CubeKrowd server. Players link their Minecraft accounts to their Discord and receive their Discord ranks.

---

### Plugin
+ The plugin allows players to type `/discord` and receive a one-time use code to be told to the Discord side of the bot.
+ The plugin allows you to disconnect by typing `/discord unlink`.
+ The plugin uses a config file that it reads from to connect to the discord bot, so connections aren't hardcoded.

---

### Discord Bot
+ The bot allows you to type `!linkup` in the `#linkup` channel with your code received on the Minecraft server to connect accounts.
+ The bot cleans up the `#linkup` channel consistently.
+ The bot allows you to type `!whois <mc-username|discordid>` to receive stored data on the player.
